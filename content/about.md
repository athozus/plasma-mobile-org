---
title: About
menu:
  main:
    parent: project
    weight: 2
--- 

Plasma Mobile is an open-source user interface and ecosystem targeted at mobile devices, built on the **KDE Plasma** stack.

A pragmatic approach is taken that is inclusive to software regardless of toolkit, giving users the power to choose whichever software they want to use on their device.

Plasma Mobile aims to contribute to and implement open standards, and is developed in a transparent process that is open for anyone to participate in.

<img src="/img/konqi/konqi-calling.png" width=250px/>

---

## Can I use it?

Yes! Plasma Mobile is packaged in multiple [distribution](/get) repositories, and so it can be installed on regular x86 based devices for testing.

Have an old Android device? [postmarketOS](https://postmarketos.org/), is a project aiming to bring Linux to phones and offers Plasma Mobile as an available interface for the devices it supports. You can see the list of supported devices [here](https://wiki.postmarketos.org/wiki/Devices), but on any device outside the main and community categories your mileage may vary. Some supported devices include the [OnePlus 6](https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)), [Pixel 3a](https://wiki.postmarketos.org/wiki/Google_Pixel_3a_(google-sargo)) and [PinePhone](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)).

The interface is using KWin over Wayland and is now mostly stable, albeit a little rough around the edges in some areas. A subset of the normal KDE Plasma features are available, including widgets and activities, both of which are integrated into the Plasma Mobile UI. This makes it possible to use and develop for Plasma Mobile on your desktop/laptop.

## What can it do?

We aim to provide an experience (with both the shell and apps) that can provide a basic smartphone experience. This has mostly been accomplished, but we continue to work on improving shell stability and telephony support.

You can find a list of mobile friendly KDE applications [here](/applications). Of course, any Linux-based applications can also be used in Plasma Mobile.

## Where can I find…

The code for various Plasma Mobile components can be found on [invent.kde.org](https://invent.kde.org/plasma-mobile).

Read the [contributors documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) for more details on the Plasma stack and how everything fits together.

You can also ask your questions in the [Plasma Mobile community groups and channels](/join).
