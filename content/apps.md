---
title: Apps
weight: 2
menu:
  main:
    weight: 6
sassFiles:
  - scss/apps.scss
header_image_link: /img/konqi/konqi-touching-plasma.png
---

Any application developed for the Linux ecosystem that has a mobile form factor is supported in Plasma Mobile.

KDE applications are generally developed with [Kirigami](https://develop.kde.org/frameworks/kirigami), but applications developed in other frameworks (ex. [GTK](https://gtk.org/)) are also compatible with Plasma Mobile!

Below is a list of applications developed by [KDE](https://kde.org) contributors that have support for a mobile form factor.

Want your project to join [KDE](https://kde.org)? See [this page](https://develop.kde.org/docs/getting-started/add-project/).

<a href='https://flathub.org'><img width='190px' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.png'/></a>

{{< apps >}}