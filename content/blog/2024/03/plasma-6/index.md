---
title: "Plasma 6 and Beyond!"
subtitle: "Plasma Mobile joins the Plasma 6 megarelease with a new shell and apps"
SPDX-License-Identifier: CC-BY-4.0
date: 2024-03-01
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2023 Luis Büchi <luis.buechi@server23.cc>
- SPDX-FileCopyrightText: 2024 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2024 Seshan Ravikumar <seshan@sineware.ca>
- SPDX-FileCopyrightText: 2024 Bart De Vries <bart@mogwai.be>
aliases:
- /2024/02/21/plasma-6/
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
images:
 - scale-1.jpg
home:
- name: Light Home
  url: 0-home-light.png
- name: Light Dark
  url: 0-home-dark.png
- name: Options
  url: 0-home-options-dark.png
- name: Settings
  url: 0-home-settings.png
- name: Widgets
  url: 0-home-widgets-dark.png
- name: Widgets edit
  url: 0-home-widgets-edit-dark.png
kalk:
- name: Calculator
  url: kalk-1.png
- name: History page
  url: kalk-2.png
- name: Settings
  url: kalk-3.png
setup:
- name: Landing
  url: setup-1.png
- name: Pre-install
  url: setup-2.png
- name: Time
  url: setup-3.png
- name: Wi-Fi
  url: setup-6.png
- name: Cellular
  url: setup-4.png
- name: Finish
  url: setup-5.png
- name: Gif
  url: setup-7.gif
auth:
- name: Dark Mode
  url: auth-1.png
- name: Light mode
  url: auth-2.png
- name: Expanded info
  url: auth-3.png
docked:
- name: Qt app before docked mode
  url: docked-1.png
- name: GTK app before docked mode
  url: docked-2.png
- name: Docked mode quick setting
  url: docked-3.png
- name: Docked mode quick setting on
  url: docked-4.png
- name: GTK app in docked mode
  url: docked-5.png
- name: Qt app in docked mode
  url: docked-6.png
vkbd:
- name: Keyboard button off
  url: vkbd-1.png
- name: Keyboard button on
  url: vkbd-2.png
settings:
- name: Cellular Dark
  url: 7-settings-cell-dark.png
- name: Cellular Light
  url: 7-settings-cell-light.png
- name: Date Dark
  url: 7-settings-date-dark.png
- name: Dark Light
  url: 7-settings-date-light.png
- name: Time Dark
  url: 7-settings-time-dark.png
- name: Time Light
  url: 7-settings-time-light.png
- name: Wi-Fi Dark
  url: 7-settings-wifi-dark.png
- name: Wi-Fi Light
  url: 7-settings-wifi-light.png
kclock:
- name: Clock
  url: 14-clock-light.png
- name: Alarms
  url: 14-clock-alarms-light.png
- name: Clock Dark
  url: 14-clock-dark.png
- name: Alarms Dark
  url: 14-clock-alarms-dark.png
kasts:
- name: Kasts Queue
  url: kasts-1.png
- name: Kasts Color Theme Settings
  url: kasts-2.png
hashomatic:
- name: Generate
  url: generate.png
- name: Compare
  url: compare.png
- name: Verify
  url: verify.png
---

The Plasma Mobile team is happy to announce the release of Plasma 6 for mobile devices!

It has been a long year of development and porting since our last major release of the shell, as well as mobile applications. This post will outline many of the highlights!

## Housekeeping

The website was refreshed in anticipation for this release. Notably, the [installation](/get) page was enhanced to better outline information about the various distributions that provide Plasma Mobile.

During the Plasma 6 development period, [postmarketOS](https://postmarketos.org/) graciously provided us with a "nightly" repository of KDE packages tracking git repositories. We have found this critically useful for development. [You can find instructions on how to use this repository here](https://wiki.postmarketos.org/wiki/Nightly).

## Plasma

[Devin wrote a blog post covering some technical details](https://espi.dev/posts/2023/12/plasma-mobile-towards-6/) covering the big aspect of the work porting all of the shell components to Qt and Plasma 6 APIs. A lot of functional work was done as well.

### Homescreen

With Plasma 6, we are once again switching back to the Folio homescreen as default. The homescreen will once again have customizable pages to place apps and widgets, as well as an app drawer and search. Devin spent time rewriting it from scratch, addressing the limitations that existed in Plasma 5, in particular the customizations are now tied to each page, no longer causing reordering issues from screen rotations.

Notable Features:

- Customizable pages to place apps and widgets
- Folders
- Drag and drop customization
- Widgets (still some known issues, see below)
- App drawer (swipe up)
- KRunner search (swipe down)
- Row-column flipping for screen rotations
- Customizable row and column counts
- Customizable page transitions
- Import and exporting of homescreen layouts as files
- and more!

{{< screenshots name="home" >}}

### Interoperability

In Plasma 5, Plasma Mobile required several config files to be installed on the system in order to set some settings needed in Plasma. For Plasma 6, Devin created a new settings service that automatically handles this situation. This allows for interoperability with Plasma Desktop being simultaneously installed on the system, and eliminates the need to install custom configuration.

### First time setup

Devin added an initial setup window that guides users to configure some basic aspects of the system, such as Wi-Fi and cellular settings.

{{< screenshots name="setup" >}}

### Authentication dialog

Devin worked on porting the authentication dialog so that it has a mobile form factor.

{{< screenshots name="auth" >}}

### Docked mode

Devin worked on adding a "docked mode" quicksetting which, when activated, enables window decorations and minimize/maximize/close buttons, and also stops enforcing application windows to be in fullscreen.

{{< screenshots name="docked" >}}

### Navigation panel

Devin added a setting to always show the keyboard toggle button. He also made it so that if there is enough screen space, the panel goes on the bottom for tablets rather than on the side.

{{< screenshots name="vkbd" >}}

### Task switcher

The task switcher was moved from the Plasma shell and into KWin to improve code maintainability.

### Vibrations

In Plasma 5, the default vibration speed was hardcoded to the PinePhone's motor, which could not register lower vibration durations. This resulted in other devices having overly strong vibration effects. Vibrations now default to an acceptable level for other devices, and can still be configured in the shell settings module.

### Flashlight

The flashlight quicksetting in Plasma 5 is hardcoded to the PinePhone. Florian worked on making it so that it now works with all phones in Plasma 6!

### Settings

Devin improved the cellular settings module so that interaction tasks are asynchronous and do not freeze the UI. He also fixed unnecessary password prompts from being shown when the module is first opened.

Devin ported the Wi-Fi settings module to mobile form components, making it consistent with other modules. He overhauled the time settings module to newer components as well, making setting the time and date more intuitive. He also did some work to ensure the UI does not freeze when applying settings.

Joshua refactored the look and feel of the Push Notifications KCM and added UnifiedPush support to NeoChat so you'll never miss a message, even when NeoChat is closed (that is, if you want to get notifications, of course!)

Mathis created a new icon for the application.

<img src="org.kde.mobile.plasmasettings.svg" width=100px />

{{< screenshots name="settings" >}}

## Known Issues

Regrettably, our team resources are limited, and so we still have several outstanding issues that were not addressed in time for this release:

- **Scrolling in Qt6 with a touchscreen has very low inertia compared to Qt5** and we cannot change the platform default, see [this](https://invent.kde.org/teams/plasma-mobile/issues/-/issues/273), reported in upstream to Qt [here](https://bugreports.qt.io/browse/QTBUG-121500)
- **Some homescreen widgets open popups** every time they are moved around
- **Limited widget selection**. Currently we just have existing widgets from the Plasma Desktop
- **The screen recording quicksetting was removed for the time being**. It needs further investigation on porting it to new APIs
- **Gesture-only mode was removed for the time being**. We need to investigate making KWin's gesture infrastructure fit our needs
- **Sometimes application windows extend under the navigation bar**. We need to investigate this regression with KWin
- **The task switcher is slow on the PinePhone in particular**. We need to investigate the performance of the KWin effect

There are other issues also being tracked in our [issue tracker](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking).

## Applications

A tremendous amount of work went into the massive effort behind porting all of our applications to Qt6 and KF6. We would like to extend a big thank you to everyone involved!

Many major announcements for applications are in the [Megarelease](https://kde.org/announcements/megarelease/6) post, so be sure to check there too!

### Photos (Koko)

While the application is still called Koko, user-facing text now calls it "Photos" to make it easier for you to find. (Devin Lin, KDE Gear 24.02, [Link](https://invent.kde.org/graphics/koko/-/merge_requests/133))

A new icon was created for the application, replacing the gorilla (Mathis Brüchert & Devin Lin, KDE Gear 24.02, [Link](https://invent.kde.org/graphics/koko/-/merge_requests/134))

<img src="org.kde.koko.svg" width=100px />

### Clock

The Clock app now pauses MPRIS media sources when an alarm or timer starts ringing and resumes (previously paused) sources once the alarm is dismissed (Luis Büchi, KDE Gear 24.02, [Link](https://invent.kde.org/utilities/kclock/-/merge_requests/109))

{{< screenshots name="kclock" >}}

### Calculator

A new configuration page was added to the calculator app, allowing to set decimal places, angle units and a parsing mode (Michael Lang, KDE Gear 24.02, [Link](https://invent.kde.org/utilities/kalk/-/merge_requests/83))

We improved the history view to allow for the removal of entries, and the drawer-style area for extra functions got replaced with a swipe view (Michael Lang, KDE Gear 24.02, [Link 1](https://invent.kde.org/utilities/kalk/-/merge_requests/78), [Link 2](https://invent.kde.org/utilities/kalk/-/merge_requests/79))

A number of new features and QoL improvements were added, including automatic font resizing when entering long expressions, better rendering of exponents, and more actual calculation features: random number, base n root/log, differential functions, standard deviations, and a lot more... Oh! and we switched math the engine to libqalculate! (Michael Lang, KDE Gear 24.02, [Link 1](https://invent.kde.org/utilities/kalk/-/merge_requests/77), [Link 2](https://invent.kde.org/utilities/kalk/-/merge_requests/75), [Link 3](https://invent.kde.org/utilities/kalk/-/merge_requests/80), [Link 4](https://invent.kde.org/utilities/kalk/-/merge_requests/73))

{{< screenshots name="kalk" >}}

## Kasts

Apart from some minor visual changes related to the Qt6 and KF6 update and several fixes regarding images and playback controls, Kasts will now update podcasts dramatically faster by not parsing RSS feeds that haven't changed (Bart De Vries, KDE Gear 24.02 [Link 1](https://invent.kde.org/multimedia/kasts/-/merge_requests/141))

The color theme (e.g. Breeze light, Breeze dark) can now be selected manually (Bart De Vries, KDE Gear 24.02 [Link 2](https://invent.kde.org/multimedia/kasts/-/merge_requests/140))

Network checks for metered connections can now be disabled altogether.  This can be useful on systems that are not able to reliably report the connection status (Bart De Vries, KDE Gear 24.02 [Link 3](https://invent.kde.org/multimedia/kasts/-/merge_requests/146))

{{< screenshots name="kasts" >}}

## Hash-o-Matic

Hash-o-Matic is a new application that lets you verify the authenticity of
files by using their MD5, SHA-256 and SHA-1 hashes or their PGP signature.

Hash-o-Matic also let you generate hash for a file and compare two files.

{{< screenshots name="hashomatic" >}}

## Contributing

Do you want to help with the development of Plasma Mobile? We are a group of volunteers doing this in our free time, and are ~~desperately~~ looking for new contributors, **beginners are always welcome**!

See our [community](/join) page to get in touch!

We are always happy to get more helping hands, no matter what you want to do, but we especially need support in these areas:

- Telephony

- Camera support

- You can also check out our Plasma Mobile [issue tracker](https://invent.kde.org/teams/plasma-mobile/issues/-/issues/) for more details.

Even if you do not have a compatible phone or tablet, you can also help us out with application development, as you can easily do that from a desktop!

Of course, you can also help with other things besides coding! For example, take Plasma Mobile for a spin to help us test bugs and triage reports! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Another option would be to help us in making these blog posts more regular again. We really don't want another 6 months of silence, but writing these take a surprising amount of time and effort.

If you have any further questions, view our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home), and consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org). Let us know what you would like to work on or where you need support to get going!

Our [issue tracker documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) also gives information on how and where to report issues.

## On a final note...

We would like to thank all of the contributors across KDE that have made this megarelease possible! We could not have done it without you. Thank you!

<img src="/img/konqi/konqi-calling.png" width="200px"/>
