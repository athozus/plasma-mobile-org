---
title: Source Code
weight: 2
menu:
  main:
    parent: project
    weight: 2
---

The most up-to-date documentation for the Plasma Mobile stack will be found on the [contributors wiki](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home).

KDE uses the [invent.kde.org](https://invent.kde.org) GitLab instance to host source code and development work.

<img src="/img/konqi/konqi-support.png" width=150px/>

---

## Plasma Mobile Shell

The Plasma Mobile shell is built on top of the Plasma stack.

The source code can be viewed at [invent.kde.org/plasma/plasma-mobile](https://invent.kde.org/plasma/plasma-mobile).

## Applications

The source code for Plasma Mobile applications by KDE can be viewed at [invent.kde.org/plasma-mobile](https://invent.kde.org/plasma-mobile).
