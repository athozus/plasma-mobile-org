---
title: Getting Plasma Mobile
menu:
  main:
    weight: 4
    name: Install
scssFiles:
  - scss/install.scss
layout: get-involved
no_text: true
---

Plasma Mobile is available to be installed on various Linux distributions.

Please check the information for each distribution to see if your device is supported.

<br />

## Distributions for ARM Devices

<div class="projects-container">
  
  <div class="project">
    <img src="/img/pmOS.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://postmarketos.org/">postmarketOS</a></h2>
    <div class="project-description">
      <p>
        PostmarketOS (pmOS), is a Alpine Linux based distribution for mobile devices. View the <a href="https://wiki.postmarketos.org/wiki/Devices">device list</a> to see the progress for supporting your device.
      </p>
      <p>
        For devices that do not have prebuilt images, you will need to flash it manually using the pmbootstrap utility. Follow instructions <a href="https://wiki.postmarketos.org/wiki/Installation_guide">here</a>. Be sure to also check the device's wiki page for more information on what is working.
      </p>
      <p>
        PostmarketOS also provides a <a href="https://wiki.postmarketos.org/wiki/Plasma_Mobile#Nightly_repository">nightly repository</a> that tracks Plasma development, and is used by developers.
      </p>
    </div>
    <p class="project-buttons-row">
      <a href="https://postmarketos.org/" class="mobile-main-button" >Learn More</a>
      <a href="https://postmarketos.org/download/" class="mobile-secondary-button">Downloads</a>
      <a href="https://wiki.postmarketos.org/wiki/Devices" class="mobile-secondary-button">All Devices</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/debian.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://mobian-project.org/">Debian (Mobian)</a></h2>
    <div class="project-description">
      <p>
        Debian provides a basic set of packages related to <a href="https://packages.debian.org/plasma-mobile">plasma-mobile</a> in its repositories since release 12, codenamed <i>bookworm</i>. The packaging effort is continuing even after the release of bookworm, allowing a broader set of packages to be available.
      </p>
      <p>
        Thanks to the efforts of the developers of <a href="https://mobian-project.org/">Mobian</a> project (a mobile derivative of Debian), flashable images based on Plasma Mobile are now available!
      </p>
      <p>
        Instructions to manually install Plasma mobile on existing Debian systems can be found on <a href="https://wiki.debian.org/Mobian/Tweaks#Plasma_mobile">Debian wiki</a>.
      </p>
    </div>
    <p class="project-buttons-row">
      <a href="https://mobian-project.org/" class="mobile-main-button" >Learn More</a>
      <a href="https://images.mobian.org/" class="mobile-secondary-button">Downloads</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/archlinux.png" class="project-logo"/>
    <h2 class="project-name"><a href="https://github.com/dreemurrs-embedded/Pine64-Arch/">Arch Linux ARM (Unofficial)</a></h2>
    <p class="project-description">
      Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.
    </p>
    <p class="project-buttons-row">
      <a href="https://github.com/dreemurrs-embedded/Pine64-Arch/releases" class="mobile-main-button" >Downloads</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/manjaro.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://manjaro.org">Manjaro ARM</a></h2>
    <p class="project-description">
      Manjaro ARM is the Manjaro distribution, but for ARM devices. It is based on Arch Linux ARM, combined with Manjaro tools, themes and infrastructure to make install images for your ARM device.
    </p>
    <p class="project-buttons-row">
      <a href="https://manjaro.org" class="mobile-main-button" >Learn More</a>
      <a href="https://forum.manjaro.org/c/arm/" class="mobile-secondary-button" >Forum</a>
      <a href="https://github.com/manjaro-pinephone/plasma-mobile/releases" class="mobile-secondary-button" >PinePhone</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/distros/kupfer.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://kupfer.gitlab.io">Kupfer</a></h2>
    <p class="project-description">
      Kupfer Linux is an Arch Linux ARM derived distribution aiming to provide tooling that makes it easier for developers to port devices to it.
    </p>
    <p class="project-buttons-row">
      <a href="https://kupfer.gitlab.io/" class="mobile-main-button">Learn More</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/distros/prolinux.png" class="project-logo"/>
    <h2 class="project-name"><a href="https://sineware.ca/prolinux/">Sineware ProLinux</a></h2>
    <div class="project-description">
      <p>
        ProLinux is a GNU/Linux distribution with an immutable root filesystem, an A/B updating scheme, Flatpak-first app distribution model, and a unified API surface to interact with the OS.
      </p>
      <p>
        <b>Plasma 6</b> development images are currently available, built from git.
      </p>
    </div>
    <p class="project-buttons-row">
      <a href="https://sineware.ca/prolinux/" class="mobile-main-button" >Learn More</a>
      <a href="http://cdn.sineware.ca/repo/prolinux/mobile/dev/arm64/" class="mobile-secondary-button" >Nightly Builds</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/openSUSE.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://opensuse.org">OpenSUSE</a></h2>
    <div class="project-description">
      <p>
        openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux distribution sponsored by SUSE Linux GmbH and other companies. Currently openSUSE provides Tumbleweed based Plasma Mobile builds.
      </p>
    </div>
    <p class="project-buttons-row">
      <a href="https://en.opensuse.org/HCL:PinePhone" class="mobile-main-button" >Learn More</a>
      <a href="https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz" class="mobile-secondary-button" >PinePhone</a>
      <a href="https://matrix.to/#/#mobile:opensuse.org" class="mobile-secondary-button" >Matrix Channel</a>
    </p>
  </div>

  <div class="project">
    <img src="/img/fedora.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://fedoraproject.org/">Fedora</a></h2>
    <div class="project-description">
      <p>
        This is a work in progress, stay tuned! Join the Fedora Mobility <a href="https://matrix.to/#/#mobility:fedoraproject.org">matrix channel</a> to get details on the progress. Links to testing images can be found in the channel.
      </p>
    </div>
    <p class="project-buttons-row">
    </p>
  </div>

</div>

<hr />

## Distributions for x86 Devices

<div class="projects-container">
  
  <div class="project">
    <img src="/img/pmOS.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://postmarketos.org/">postmarketOS</a></h2>
    <div class="project-description">
      <p>
        postmarketOS is able to be run in QEMU, and thus is a suitable option for testing a fully pre-configured Plasma Mobile environment on your computer.
      </p>
      <p>
        Read more about it <a href="https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)">here</a>. During the setup process, select Plasma Mobile as the desktop environment.
      </p>
    </div>
    <p class="project-buttons-row">
    </p>
  </div>

  <div class="project">
    <img src="/img/fedora.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://fedoraproject.org/">Fedora</a></h2>
    <div class="project-description">
      <p>
        Fedora has Plasma Mobile and related applications packaged in their repositories.
      </p>
      <p>
        Install the <a href="https://src.fedoraproject.org/rpms/plasma-mobile">plasma-mobile</a> package.
      </p>
    </div>
    <p class="project-buttons-row">
    </p>
  </div>

  <div class="project">
    <img src="/img/debian.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://www.debian.org/">Debian</a></h2>
    <div class="project-description">
      <p>
        Plasma Mobile is available in Debian unstable as the <a href="https://packages.debian.org/sid/plasma-mobile">plasma-mobile</a> package.
      </p>
    </div>
    <p class="project-buttons-row">
    </p>
  </div>

  <div class="project">
    <img src="/img/archlinux.png" class="project-logo"/>
    <h2 class="project-name"><a href="https://archlinux.org/">Arch Linux</a></h2>
    <div class="project-description">
      <p>
        Plasma Mobile is available on the <a href="https://aur.archlinux.org/packages/plasma-mobile">AUR</a>.
      </p>
    </div>
    <p class="project-buttons-row">
    </p>
  </div>

  <div class="project">
    <img src="/img/neon.svg" class="project-logo"/>
    <h2 class="project-name"><a href="https://neon.kde.org/">KDE Neon</a></h2>
    <div class="project-description">
      <p>
        <b>WARNING</b>: This is not actively maintained!
      </p>
      <p>
        This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines. It can be <a href="https://files.kde.org/neon/images/mobile/">downloaded here</a>.
      </p>
    </div>
    <p class="project-buttons-row">
    </p>
  </div>

</div>
