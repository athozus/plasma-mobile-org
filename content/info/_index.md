---
title: Legacy Releases
menu:
  main:
    parent: project
    weight: 8
---

**Note:** Plasma Mobile Gear has been discontinued. Applications that were previously part of it are now shipped with KDE Gear, or have independent releases.

This is the list of historical Plasma Mobile Gear releases with tarball links, which was specifically for Plasma Mobile **applications**.

Plasma Mobile shell releases are alongside the regular Plasma releases.

See the [release schedule page](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Release-Schedule) for more details.
