---
version: "22.04"
title: "Plasma Mobile Gear 22.04"
type: info
date: 2022-04-23
---

In this release following repositories are marked as unstable,

- kclock
- krecorder
- qmlkonsole
- tokodon
- plasmatube
- khealthcertificate
- vakzination

Some of this repositories are in process of kdereview, and will be marked stable in upcoming releases.

# Changelog

{{< details title="alligator" href="https://commits.kde.org/alligator" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/alligator/0185fe03ebabbb128ae1d72c600985a56d21e7c2) 
+ Add Windows CI. [Commit.](http://commits.kde.org/alligator/331012639f63bece38ae020c0ad3e6e621cec316) 
+ Don't use contextProperty to make KAboutData available on QML. [Commit.](http://commits.kde.org/alligator/62f6c6faca9dbd6a91210c2219a8d39717d9d266) 
+ Use "#pragma once". [Commit.](http://commits.kde.org/alligator/4922c7967c48dcfc982cddf329da3b3b4e7cbfae) 
{{< /details >}}

{{< details title="angelfish" href="https://commits.kde.org/angelfish" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/angelfish/68488928d18e2ba9eac2215e6226c4eb1175a8b1) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/a13407b28c299c32a777c3ff0a0e82c04a4cb858) 
+ Webapp: Create desktop files with Type=Application. [Commit.](http://commits.kde.org/angelfish/88d742733e1a0b1f0918400db83deb0dd4928b81) 
+ Update adblock crate to 0.5. [Commit.](http://commits.kde.org/angelfish/99b17a40cb7448986eb001574d93baa4504c3b00) 
+ Regenerate flatpak sources. [Commit.](http://commits.kde.org/angelfish/f07df044daefcc74e39caf6be68d197dd8d4e8d5) 
+ Update to adblock 0.4. [Commit.](http://commits.kde.org/angelfish/c3243da4c939b33e21e0b95ec594d35acb60e6d0) 
+ Desktop: Expose Downloads. [Commit.](http://commits.kde.org/angelfish/f6ce8f8cf08a0263a319be882782883b521e0e4c) 
{{< /details >}}

{{< details title="audiotube" href="https://commits.kde.org/audiotube" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/audiotube/1ad6586a07b4397fad36af1ad455f7a24ce5104e) 
+ Flatpak: Update urllib. [Commit.](http://commits.kde.org/audiotube/1a93e0d944f762f977bf9c25e67721ddcb4f3a4d) 
+ Update ytmusicapi to 0.21.0. [Commit.](http://commits.kde.org/audiotube/347613a7f26fb1ce340560eaf12ea92749355c98) 
+ MultiIterableView: Add convinience std::vector constructor. [Commit.](http://commits.kde.org/audiotube/961dca328d847f7434701e4bcc17534afaace3e4) 
+ Power search page list sections. [Commit.](http://commits.kde.org/audiotube/433d95969c3a97d3ef57a421c44957a8cca08ecd) 
+ Simplify std::sort use. [Commit.](http://commits.kde.org/audiotube/15f439c38ee22ea86ee8452cd4f06d535bc114de) 
+ MultiIterableView: Add documentation. [Commit.](http://commits.kde.org/audiotube/ca4d5d8b778d9195ddabb2bbbce07f4eee0ed02f) 
+ Fix some implicit this captures. [Commit.](http://commits.kde.org/audiotube/06338900dc742810fbe4a803b9ab008d0ca1b140) 
+ MultiIterableView: Generalize by using std::span. [Commit.](http://commits.kde.org/audiotube/8ec674fbef0a1db49f32c4ff94678d599f5f99b0) 
+ ArtistModel: Simplify use of MultiIterableView. [Commit.](http://commits.kde.org/audiotube/58eefc29ebf3473bebb92182abcc9bd932d3c50d) 
+ MultiIterableView: Refactor into an actual view type. [Commit.](http://commits.kde.org/audiotube/93123317551e78ae2610a19682b4eae0d007912e) 
+ MultiIterableView: Try to make code easier to understand. [Commit.](http://commits.kde.org/audiotube/1d25f46f52ed787e2bf1d19f354066085c90d491) 
+ Flatpak: Update pybind11. [Commit.](http://commits.kde.org/audiotube/a775cd58be25d6e1ff79d2f20122e1cd9813e534) 
{{< /details >}}

{{< details title="calindori" href="https://commits.kde.org/calindori" >}}
+ Add qtquickcompiler to speed up launch times. [Commit.](http://commits.kde.org/calindori/554be1d1fd8504788b29587bc70abaab6ccab44f) 
+ Ensure theme fallback to kde style on non-Plasma sessions. [Commit.](http://commits.kde.org/calindori/cca0e4778f911b305ff262bf1a0908dd43742253) 
+ Update WeekPage.qml - Fix capitalization consistency with other pages (e.g. "Show Completed" on Task page). [Commit.](http://commits.kde.org/calindori/5363f6e24f98ad81ba9ecd035da58be8bac85931) 
{{< /details >}}

{{< details title="kalk" href="https://commits.kde.org/kalk" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/kalk/0fc8d088c6265ea1bf1e75426656690495c9eeb8) 
+ Add clang format. [Commit.](http://commits.kde.org/kalk/ab8e8a9f9fedbc285fdcf959f01f4f0d89807695) 
+ Check size before pop. [Commit.](http://commits.kde.org/kalk/cf79eec8dbc482ff55b561cfaacaf2d7c4ac84ee) 
+ Disallow double `-` or `+`. [Commit.](http://commits.kde.org/kalk/f205dedf40c8b6ff7e6c41484591b388076f0eab) 
+ Appdata: Remove duplicated metadata_license. [Commit.](http://commits.kde.org/kalk/d7f762a69e36dbe29c17a1eb0b13bbb12b3fc6f8) 
+ Don't use contextProperty to make KAboutData available on QML. [Commit.](http://commits.kde.org/kalk/89afe5667565ad67fc352cfaa1dffb778e6d0ef4) 
+ Add qtquickcompiler to speed up launch time. [Commit.](http://commits.kde.org/kalk/f14f1630e0f916aa877c812169d7fab7a1a01017) 
+ Ensure there is fallback to kde theme if none is set. [Commit.](http://commits.kde.org/kalk/b5694d935d59788dd79ef057f8f310be5a0e9d7c) 
{{< /details >}}

{{< details title="kasts" href="https://commits.kde.org/kasts" >}}
+ Use undeprecated install dirs. [Commit.](http://commits.kde.org/kasts/0ccf7735422201d9a86b8839c398a476ee645622) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/kasts/7887ff2cc9eaa7f852bd71bce0bbab660b5ea584) 
+ Add windows CI. [Commit.](http://commits.kde.org/kasts/cccf70854d9929f4411ae99144871c7f5e0cf9a8) 
+ Enable highdpi scaling. [Commit.](http://commits.kde.org/kasts/d98bb3cc96bdc8c66fe59ecfa4d4d4f37af9d3ed) 
+ List the package names on debian. [Commit.](http://commits.kde.org/kasts/5dd19faed9faf68cd4917c3014c4df58196d0e45) 
+ Show loading notification and add keywords to desktop file. [Commit.](http://commits.kde.org/kasts/8c116ab5dd83789f487c75e35374d0d36a321ebc) 
+ Keep unreadEntryCount cached instead of getting it from DB everytime. [Commit.](http://commits.kde.org/kasts/bbded05933add0f477b0660d6224e9728583541e) 
+ Set QT_ENABLE_GLYPH_CACHE_WORKAROUND=1 if it is not set. [Commit.](http://commits.kde.org/kasts/529a1ca878fad125c1844af043cdb64c0d044cb7) 
+ Port to Kirigami.Dialog. [Commit.](http://commits.kde.org/kasts/36d9a84bffd744e905d414903aa8946a3fb5556b) 
{{< /details >}}

{{< details title="kclock" href="https://commits.kde.org/kclock" >}}
+ Add Qt6 CI. [Commit.](http://commits.kde.org/kclock/d801ebb70776c3f7da0a8e1526b002ba1d62e07d) 
+ Port alarm playing to Qt6. [Commit.](http://commits.kde.org/kclock/f0982dc08fdb6fe376f40f3b0b41b2f71c33a5b4) 
+ Fix comparing QVariants in Qt6. [Commit.](http://commits.kde.org/kclock/d935217ab89d4d4b8b4cf1ff964fc8d7d4ca2df6) 
+ Port applet away from deprecated things. [Commit.](http://commits.kde.org/kclock/959bcfff36ef854389a1365669bddc8cdac361cf) 
+ Use proper install dirs. [Commit.](http://commits.kde.org/kclock/c05ec26d5a2ed24aaa58be0342af6d652bd663ca) 
+ Adapt build system to Qt6. [Commit.](http://commits.kde.org/kclock/a2de7a8ea84457b779e3f3fba19a3677c26c68fa) 
+ Use undeprecated install dirs. [Commit.](http://commits.kde.org/kclock/fa85cad4c94ef062f0c7de7ed69c782e77407c99) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/kclock/e4dabf80d994e679d7f6bf81ddcb22fffe596be1) 
+ Fix next queued wakeup not being scheduled with wait worker thread. [Commit.](http://commits.kde.org/kclock/848610ab05faa851eb3e9db1985f12cb431252d0) 
+ Fix wait worker thread scheduling infinite wakeups due to timerfd behaviour with max numeric_limits. [Commit.](http://commits.kde.org/kclock/b31d1337f4d05a5ca7ba2a8e08c26b3ef8fe03bc) 
+ Fix kclockd CLI arguments. [Commit.](http://commits.kde.org/kclock/e843b96281b0e1d9cc68c4bfdae9017b200b5c42) 
+ Cleanup kclockd worker threads, and fix list removal out of bounds. [Commit.](http://commits.kde.org/kclock/4917e08ff8de3ea588fcde4a74eed03aab5c5bd3) 
+ Don't use contextProperty to make KAboutData available on QML. [Commit.](http://commits.kde.org/kclock/94ddac85d5a3ec11e63fc6a07e24ee939431534b) 
+ Use qtquickcompiler to speed up launch times. [Commit.](http://commits.kde.org/kclock/c076ea0794adb7661f26b54a7fa70fc105bdbf12) 
+ Add org.freedesktop.portal.Background support for Flatpak. [Commit.](http://commits.kde.org/kclock/bcee4f1a052356f9198e1e0f119987ab9ed73adf) 
+ Fix SettingsModel call. [Commit.](http://commits.kde.org/kclock/ce6227216e2dc7076b323d30c3488b6553c18c2e) 
+ Switch context properties to singletons. [Commit.](http://commits.kde.org/kclock/d83ac15bf5eda24953afd1e7dde5cd590def7803) 
+ Use KDE widget style if possible when not on plasma. [Commit.](http://commits.kde.org/kclock/1444899b13cf800196286882de3685ba64d4b492) 
+ Remove automatic kclockd shutdown to due client issues. [Commit.](http://commits.kde.org/kclock/c5ddf9e87f171f62a81050a2b845c02a9d6df2f6) 
+ Add automatic version string to kclockd. [Commit.](http://commits.kde.org/kclock/f7cfeeb5faa534f39c8ae9da66d7827ac38b5b62) 
+ Add timer popup when ringing. [Commit.](http://commits.kde.org/kclock/e86c048c0e24355f7cf649f0d207dd4b270f8b8e) 
+ Refactor timers and added ringing property. [Commit.](http://commits.kde.org/kclock/73f5c97902e31f54cf0510af268828adbbae9bb2) 
+ Ensure alarms do not ring after deletion. [Commit.](http://commits.kde.org/kclock/e7d767a79f66ff8d3936429e74a5679a66b99f3a) 
+ Cleanup and refactor to improve kclockd code quality. [Commit.](http://commits.kde.org/kclock/3a53d7ea8250e6ead6fe657843750f622f678de7) 
+ Use consistent numbering style for time page delegates. [Commit.](http://commits.kde.org/kclock/b4f8745ffb380087cfca93d0ebe3e3df5cf74052) 
+ Update copyright year. [Commit.](http://commits.kde.org/kclock/9f58ccc5c6372171beb4649b6d835f0b1adeebbe) 
{{< /details >}}

{{< details title="keysmith" href="https://commits.kde.org/keysmith" >}}
+ Remove minSdk from Android manifest. [Commit.](http://commits.kde.org/keysmith/e2cd56e5361a4801fb59bb790cebe458f7a521da) 
{{< /details >}}

{{< details title="khealthcertificate" href="https://commits.kde.org/khealthcertificate" >}}
+ Use undeprecated install dirs. [Commit.](http://commits.kde.org/khealthcertificate/edd1c57d670956723c33c043310ccb38f2a32c45) 
+ Require unit tests to pass for the CI to pass. [Commit.](http://commits.kde.org/khealthcertificate/52fb7cfad9e0af84039826c78fd4a87ef74428c7) 
+ Add basic support for domestic Danish COVID certificates. [Commit.](http://commits.kde.org/khealthcertificate/31d2f27c3c545520b62b707fc5839d8a43613575) 
{{< /details >}}

{{< details title="koko" href="https://commits.kde.org/koko" >}}
+ Add Qt6 CI. [Commit.](http://commits.kde.org/koko/b0a249c18045c348d83cb11638ce304d0bd34cd2) 
+ Port away from deprecated datetime enums. [Commit.](http://commits.kde.org/koko/68c3b4faa8c8a030ce6ed521b909dbe65ac6cf42) 
+ Remove unused ImageDocument class. [Commit.](http://commits.kde.org/koko/4d7e9621f9032857dae9b9e7553f9389b7227807) 
+ Port away from deprecated QStandardPaths enums. [Commit.](http://commits.kde.org/koko/021a08d2e094f41a6c8b1fa1b1cdd89827505811) 
+ Adapt build system to Qt6. [Commit.](http://commits.kde.org/koko/2b31c9f0f8fc6c7377e4c3eb05f13d1fc20c49ca) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/koko/78332bc1b812f76f8cd1a85e18a170986aa2ad89) 
{{< /details >}}

{{< details title="krecorder" href="https://commits.kde.org/krecorder" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/krecorder/dcb716cb56dbec3924ca5642c75ed0c040be90d2) 
+ Don't use contextProperty to make KAboutData available on QML. [Commit.](http://commits.kde.org/krecorder/56d761da4704a78917b6ad5337556dc5af926e31) 
+ Add qtquickcompiler to speed up launch times. [Commit.](http://commits.kde.org/krecorder/171532bd97bd203becb1fbf4445e19922ec6c743) 
+ Ensure we use kde theme as fallback on non-Plasma sessions. [Commit.](http://commits.kde.org/krecorder/683c11787b29c6ed8e898aca7b785eda724bb9a5) 
+ Update copyright year. [Commit.](http://commits.kde.org/krecorder/431761985c2014c04828b6f38a052dbd034ea0e2) 
{{< /details >}}

{{< details title="ktrip" href="https://commits.kde.org/ktrip" >}}
+ Use undeprecated install dirs. [Commit.](http://commits.kde.org/ktrip/81428dc737d152c0eccadd074c674baf8362aad6) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/ktrip/b4b9876af4434fab40044a56a8787ac8cf249785) 
+ Fix bug reporting address. [Commit.](http://commits.kde.org/ktrip/0ca53084a95da73000c4260f333a322a49423d6d) 
+ Add 150px app icon. [Commit.](http://commits.kde.org/ktrip/dc230c49643608a7585b161874b448a7ae27079f) 
+ Enable highdpi scaling. [Commit.](http://commits.kde.org/ktrip/63b0f61bbe48a69af5c5979b496e39cc1646f0a5) 
+ Add windows app icon. [Commit.](http://commits.kde.org/ktrip/ed650eceef7408a9934623c097123629e422b780) 
+ Don't use KDEFrameworkCompilerSettings. [Commit.](http://commits.kde.org/ktrip/3459a376ca1a3df629d97676cdf2e9fcebb5eccb) 
{{< /details >}}

{{< details title="kweather" href="https://commits.kde.org/kweather" >}}
+ Remove unused ProgressCircle.qml. [Commit.](http://commits.kde.org/kweather/897e3d0455ef3b81a577bd6bd5409932296f3966) 
+ Use Kirigami Dialog instead of local implementation. [Commit.](http://commits.kde.org/kweather/c0eb123738b4412e4a4287b902bce901e3fa89c4) 
+ Refactor Settings dialogs into separate component. [Commit.](http://commits.kde.org/kweather/c7bcbf5b59df2fab53613ce03b09c09c3b363de2) 
+ Fix: show mmHg when using that setting. [Commit.](http://commits.kde.org/kweather/6e24d73f59d04fd09913a9c17ef9b22e4415efda) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/kweather/924e5b0a4173f1d625c54ae1ffddeb61236b740f) 
+ Use qtquickcompiler to speed up application launching. [Commit.](http://commits.kde.org/kweather/f320de6f2ab39c7673288099d7df7ccc3d713679) 
+ Ensure there is fallback to kde theme if none is set. [Commit.](http://commits.kde.org/kweather/2663f2fec23924996a25ab9b2e4ca11a06213bf0) 
{{< /details >}}

{{< details title="neochat" href="https://commits.kde.org/neochat" >}}
+ Use undeprecated install dirs. [Commit.](http://commits.kde.org/neochat/709b2c8fd907d1ca23e9d27a50269d145dd6341d) 
+ Fix mis-aligned user messages. [Commit.](http://commits.kde.org/neochat/d0bc8f3d0515d5ceec4ccf6e9756cb120998cabb) 
+ Require passing tests on CI. [Commit.](http://commits.kde.org/neochat/0f5425e030f793118199351504c5e6fe8d82cc55) 
+ Close WelcomePage after account is loaded. [Commit.](http://commits.kde.org/neochat/f381cc462377bb0e4d280d473678c778efc57e3c) 
+ Disable busyindicator. [Commit.](http://commits.kde.org/neochat/decd5280799358e4b73b5f231490b30121090e9d) 
+ Fix REUSE check on CI. [Commit.](http://commits.kde.org/neochat/0c5bd579763e48e5dbc1d070ae94a60d7e663a08) 
+ Don't try to load more messages than there are in the timeline. [Commit.](http://commits.kde.org/neochat/7362b90c42f4382f8718f8a6eece5cc0bf6a2f9c) 
+ More typing notification improvements. [Commit.](http://commits.kde.org/neochat/aef6d6fc8553745abcbef75d17ae884324d6bacc) 
+ Try fixing stuck read notifications. [Commit.](http://commits.kde.org/neochat/432e209b16f37ae1524e7816f177ddb59a6ca0c7) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/neochat/b9152dc93cd03e6680a2f1fed5b91ee3b7dd6314) 
+ Fix link. [Commit.](http://commits.kde.org/neochat/c1576256455a2390d10294c70f1e5fc4cbdda4f5) 
+ Add Windows CI. [Commit.](http://commits.kde.org/neochat/026c7660bc17a4555a6f41c98c00ea6885b0c9dd) 
+ Fix condition to build runner. [Commit.](http://commits.kde.org/neochat/be10e669741c6e233b1e4c74fa0320fc77ae8228) 
+ Add rooms runner. [Commit.](http://commits.kde.org/neochat/1cc8d915bc57272cc112ef965ae9991ee6745cde) 
+ Show subtitle text without markdown. [Commit.](http://commits.kde.org/neochat/9a5f2e49388e4200c1a19228b21ab0e1b4aea9a3) 
+ Set preferredWidth and preferredHeight of images. [Commit.](http://commits.kde.org/neochat/334c13b36c465de44188d7c1b14c14bffae9c0ea) 
+ Revert "Show RoomList when cached state is loaded". [Commit.](http://commits.kde.org/neochat/aac96da2e27be4da27e0d69861717cdc6f029512) 
+ Lower typing notification timeouts. [Commit.](http://commits.kde.org/neochat/12f3f72a679a6a42cd60475b15717da2fc1880cf) 
+ Force RoomListDelegate to use plaintext. [Commit.](http://commits.kde.org/neochat/62f6cfbf9a0cb4824d3b05042c6cd4baea04fc9d) 
+ Disable the BusyIndicator. [Commit.](http://commits.kde.org/neochat/9252e0e65e2df311217ca017501f69ec846d6fa7) 
+ Apply 1 suggestion(s) to 1 file(s). [Commit.](http://commits.kde.org/neochat/80ee5e9356ef6c792820052cced585078f7ee694) 
+ Make invitation notifications persistent. [Commit.](http://commits.kde.org/neochat/be802a28c2120ce7ba9feae043cd98356a5a1d9a) 
+ Don't apply autocompletion when autocomplete list is empty. [Commit.](http://commits.kde.org/neochat/b2a8430fa2c532e1b280d06c6a7a504ff9b88cfc) 
+ Show RoomList when cached state is loaded. [Commit.](http://commits.kde.org/neochat/db5f3285395c52dca16be92dcecbeeaa88a74fba) 
+ Add nicer delegate message for widget events. [Commit.](http://commits.kde.org/neochat/022951a9dfc3bf508f3cf271c894053240a59740) 
+ Fix quitting without tray icon. [Commit.](http://commits.kde.org/neochat/47a0d30e570f3e6a40e53290f957e6aa8a70cda1) 
+ Prepare Image & Video loading for E2EE. [Commit.](http://commits.kde.org/neochat/faeb1964bd552bd26d96e6d080cefb25bcfa0b56) 
+ Aggregate similar state events. [Commit.](http://commits.kde.org/neochat/db8b2fd64bcac2f582fe3003253f5d27b7097f1e) 
+ Don't load backlog until read marker. [Commit.](http://commits.kde.org/neochat/37c7fe380b5c22532b583896067c639c55946954) 
+ Fix login regex. [Commit.](http://commits.kde.org/neochat/dc9d574b589e38a4614c701b28bbb0a61ff46dd3) 
+ Modifies regex check for valid matrix server to accept ip addresses. [Commit.](http://commits.kde.org/neochat/7c807e6a250ad349f3c3fbbf44ce4e699ac518ab) 
+ Wraps the checkbox text for messages on the right to wrap on mobile devices (PinePhone). [Commit.](http://commits.kde.org/neochat/f74c6a41aebd7b01f28ccfc866975abf882a772c) 
+ Wraps the quick edit checkbox using workaround. [Commit.](http://commits.kde.org/neochat/7d5a8c87a107f342b2fd958d71d27cc5fb5c7ba0) 
+ The component of QtCoro5 is called Core and not Coro ;). [Commit.](http://commits.kde.org/neochat/ca719b835ed264d327593abdc40ff6d332f4c089) 
+ Uses the formatted message to enable clickable links for mobile. [Commit.](http://commits.kde.org/neochat/fdfbbb1b04b703dc2351d8b0879d5087f3c65f6b) 
+ Load replied-to message when it isn't in the timeline already. [Commit.](http://commits.kde.org/neochat/dd91cb91d0ac9ce4073c6e24b9ff7ca1961505d6) 
+ Port away from CMake deprecation. [Commit.](http://commits.kde.org/neochat/290b2249c457c1c419756303afd631445126cd6d) 
+ Fix issue with clear image button. Will only be visible if the user has an avatar (local or saved). [Commit.](http://commits.kde.org/neochat/8b8e521c5657bb58c5bc569d78a8d43e43b1cc23) 
+ Allow disabling notification inline reply. [Commit.](http://commits.kde.org/neochat/cba88e1af7d17c06fdb683c740a07dcfe6b16664) 
+ Use Quotient's NetworkAccessManager in QML. [Commit.](http://commits.kde.org/neochat/1661d34d7c61152617d75e4d30bbe223f14434c3) 
+ Remove unneeded parameter. [Commit.](http://commits.kde.org/neochat/dc3b1a3c876ecf7f2b5803226e4bbe3d8fdf9e70) 
+ Make user colors update when colortheme changes. [Commit.](http://commits.kde.org/neochat/f55dc19d954c65c61dc994fbe0569060c0aa31df) 
+ SingleMainWindow is a part of XDG SPEC version 1.5 and bogus on 1.0. [Commit.](http://commits.kde.org/neochat/6014c15b4f45e54fbc741f6e444a1aab07e2e999) 
{{< /details >}}

{{< details title="plasma-dialer" href="https://commits.kde.org/plasma-dialer" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/plasma-dialer/3ff206aa05442d3d3358a815a23aa088b5f8afdb) 
+ Kde-telephony-daemon: port to callaudio. [Commit.](http://commits.kde.org/plasma-dialer/7b7062ae0bb52411ac1d9d0fb2f7542e1b7c382e) 
+ Notification: connect to activation after setting actions. [Commit.](http://commits.kde.org/plasma-dialer/8938f78bcac925698a5ed7015fa8d20a6150a32d) 
+ [plasma-dialer] Improve dialpad call button scaling. [Commit.](http://commits.kde.org/plasma-dialer/01308ae7cfe75a95296771ebdde3ca763dad1ba5) 
+ [plasma-dialer] Fix sidebar init regression. [Commit.](http://commits.kde.org/plasma-dialer/818091ddd5236fee213d1d83f7665d8971c9eb94) 
+ [plasma-dialer] Refactor toolbar and sidebar switch. [Commit.](http://commits.kde.org/plasma-dialer/2a48e222cfdf7df11364a7de5808654c80a188ab) 
+ [plasma-dialer] Fix "clear history" dialog height logic. [Commit.](http://commits.kde.org/plasma-dialer/78e9b37080ac4b6eb02bc4dd8de51ef9b5c0467d) 
+ [plasma-dialer] Reorganize and cleanup QML files. [Commit.](http://commits.kde.org/plasma-dialer/fdd0705545d2683b0f1235bd1f9d86dc9f8604ca) 
+ [plasma-dialer] BottomToolbar QML cleanup. [Commit.](http://commits.kde.org/plasma-dialer/b430044a0b70cac66d0d2a5ac762018864fcfa04) 
+ [plasma-dialer] Replace uses of appwindow with applicationWindow(). [Commit.](http://commits.kde.org/plasma-dialer/c69b4e21904f49a4697fb3ffbac1f85b1b075486) 
+ [plasma-dialer] Fix misaligned sidebar header with different fonts. [Commit.](http://commits.kde.org/plasma-dialer/d866d43345bcc8c10f060b9e8bdbf92816f86304) 
+ [plasma-dialer] Improve interaction with in-call dialpad. [Commit.](http://commits.kde.org/plasma-dialer/93759b93d26adcabd209b46a2daad38590fa199d) 
{{< /details >}}

{{< details title="plasma-phonebook" href="https://commits.kde.org/plasma-phonebook" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/plasma-phonebook/3dc5338999608a736fd56a0c4a21907e4daa8600) 
{{< /details >}}

{{< details title="plasma-settings" href="https://commits.kde.org/plasma-settings" >}}
+ Rename kpackage to kcm_mobile_virtualkeyboard. [Commit.](http://commits.kde.org/plasma-settings/649c27a47514e079433beccca41a5093ff4853d6) 
+ [cellularnetwork] Use Kirigami.Dialog and mobile form style. [Commit.](http://commits.kde.org/plasma-settings/680f96fa104eb9954413ee57d6743c8c816507cc) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/plasma-settings/f2b9dccee642cd72c59fa8ae4ef293ae5381fd8a) 
+ Rename the virtual keyboard kcm to On-Screen keyboard. [Commit.](http://commits.kde.org/plasma-settings/a7b1bf7ace1f4198114072bf02c0526e6c3f1bf5) 
+ [powermanagement] Ensure KConfigWatcher is notified on save. [Commit.](http://commits.kde.org/plasma-settings/a0fafc9ec2f7f68e8ca0fb4152b27e85d8e02fae) 
+ [powermanagement] Ensure config is set so screen is locked when dpms off. [Commit.](http://commits.kde.org/plasma-settings/5c0112df1ab53783a93cf49521a7e7565f51558f) 
{{< /details >}}

{{< details title="plasmatube" href="https://commits.kde.org/plasmatube" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/plasmatube/7e9c33ace2bbff45c2f057b5e8a4378601f168e1) 
+ Fix reuse compatibility. [Commit.](http://commits.kde.org/plasmatube/ea4bed0e09722d38d2e71f7ca24c173de402b821) 
+ Add button to change video format. [Commit.](http://commits.kde.org/plasmatube/20247f9daf3e9f4bb4ccb024d693e357d30ed8ad) 
+ Add settings page to configure invidious instance. [Commit.](http://commits.kde.org/plasmatube/f1e1c55f4b35274d6dd1f1307d59eeaeffb535aa) 
+ Add CI. [Commit.](http://commits.kde.org/plasmatube/86100c0b52863b6f01ec9f0292d9bcacbe2d0207) 
+ Require CMake 3.16. [Commit.](http://commits.kde.org/plasmatube/2b7687dd53e19fef56b01667fc8dd37051a835a2) 
{{< /details >}}

{{< details title="qmlkonsole" href="https://commits.kde.org/qmlkonsole" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/qmlkonsole/8f22183de903d3af36257890594d9b26d37190e1) 
{{< /details >}}

{{< details title="spacebar" href="https://commits.kde.org/spacebar" >}}
+ Fix mobile scrolling and make quick scroll buttons larger. [Commit.](http://commits.kde.org/spacebar/35adae9affafdc333de6135974fdae2c2e461240) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/spacebar/5133717470f0893efe66e95b075f15186d8cc4f3) 
+ Update README.md app section description. [Commit.](http://commits.kde.org/spacebar/bfa09f18e0d7c495b6f042abba834ccbdfe7556a) 
+ Minor tweaks to UI. [Commit.](http://commits.kde.org/spacebar/e6c377c27926b4166cef76c0e2de05b2c1d47bad) 
+ Fix duplicate action trigger for message send. [Commit.](http://commits.kde.org/spacebar/6c53efbf1dd9f046fbc3dde8378ad113342ddc0e) 
+ Mark incoming messages as read for open chat conversations. [Commit.](http://commits.kde.org/spacebar/1d90ebe774e9c6d053ecf3768add7fe9ffb9fe99) 
+ Make links in messages clickable/touchable part 2 (#47). [Commit.](http://commits.kde.org/spacebar/038dd60226a7a8ca17278e5ddf321f5d21bdf854) 
+ Make links in messages clickable/touchable (#47). [Commit.](http://commits.kde.org/spacebar/84cf6ed3bb742ad51884436c802bd562b51ea20d) 
+ Refactor contact selection and implement support for choosing of phone number via contacts. [Commit.](http://commits.kde.org/spacebar/baeed009d0cc5d791541a704109f0212a4f66d01) 
+ Generate random Id for sent messages rather than using the modem message path... [Commit.](http://commits.kde.org/spacebar/3540c34b9ed76d26671b34a972a550be2645df75) 
+ Add notification setting options to hide/show the sender and the message content. [Commit.](http://commits.kde.org/spacebar/b4d354d3dba0b432a7c37b47e125e8d6e8246e06) 
+ Refactor the add attachment layout. [Commit.](http://commits.kde.org/spacebar/a54f0b8b8f5ffe474863e500bddc72431859b573) 
+ Keyboard shortcut for sending message with "ctrl+enter". [Commit.](http://commits.kde.org/spacebar/3f1f9cdd0bcf3ad13e0f88c2833cee42d2eec2c6) 
+ Change header style to toolbar, flatten app navigation, and minor UI improvements. [Commit.](http://commits.kde.org/spacebar/76a85666f1fc99867273f018649c8a80284caefd) 
{{< /details >}}

{{< details title="tokodon" href="https://commits.kde.org/tokodon" >}}
+ Update project url. [Commit.](http://commits.kde.org/tokodon/01f141c995e6e67295b0dd6ab4c7f40649ec1321) 
+ Simplify navigation button. [Commit.](http://commits.kde.org/tokodon/f52148fbc894c4efc6aa43b3bfbeb30a0a3db016) 
+ Fix fetching statues from account with id > 32 bits. [Commit.](http://commits.kde.org/tokodon/5a3a65ae5ea70a854475c8ffed48a4f30e064660) 
+ Unify AccountModel and TimelineModel fetchMore() logic. [Commit.](http://commits.kde.org/tokodon/7f9d370e34ebe1d2183b94585632ed418d3598d0) 
+ Better handle own account in profile view. [Commit.](http://commits.kde.org/tokodon/8de6558fd5a7de3365a77e75bbbc9549a1ed8107) 
+ Fix general page missing title. [Commit.](http://commits.kde.org/tokodon/07e4c5be37436a668f2a844311831c5e7be0e34e) 
+ Fix copy paste issue. [Commit.](http://commits.kde.org/tokodon/00bdb5ed65bc5ce094bbb833e3cfbc713293501b) 
+ Improve desktop mode. [Commit.](http://commits.kde.org/tokodon/47869792402eb7c3ff227e635b9b3ce16d033856) 
+ Improve REUSE compliance. [Commit.](http://commits.kde.org/tokodon/5221364649167363ff33819c3b855ea4cf29a849) 
+ Add ki18n_install. [Commit.](http://commits.kde.org/tokodon/faa13244baceffb8e1c46558c7e7e5784ba45508) 
+ Fix minor typo. [Commit.](http://commits.kde.org/tokodon/0932587b3a0a0b0207a2098e1a84c9597d8735ae) 
+ Implement more account actions. [Commit.](http://commits.kde.org/tokodon/1ec3bdb7eb05cad0c793caec995fe86b3f333056) 
+ Improve look of the personal fields. [Commit.](http://commits.kde.org/tokodon/310f41ae99d1c515843c816e8ac846cddee28946) 
+ Fix profile fieds. [Commit.](http://commits.kde.org/tokodon/d6a2c9d8867c18a2ccbcd51c6a85d7ef71ce6fbf) 
+ Fix time representation for hours and minutes < 10. [Commit.](http://commits.kde.org/tokodon/d5fe5f2a89d52aa6af7e69b3e231c4bece1f1218) 
+ Use qint64 for account id. [Commit.](http://commits.kde.org/tokodon/2f49cf3bd8450c5280131a5bae2be933beaa1349) 
+ Attach "Follow" button to a corresponding network request. [Commit.](http://commits.kde.org/tokodon/7540c89bbc2cb59778ca123ed478d62d42b7578f) 
+ Silent i18n warning about empty string. [Commit.](http://commits.kde.org/tokodon/12bfe2ee837af58dce93c4c7f9bb673c4f1ae16a) 
+ [appstream] Add screenshots. [Commit.](http://commits.kde.org/tokodon/f97a5e8862a269c5dbab85508ce2b9c6b1b08056) 
+ Add tooltips to toot-composer option buttons. [Commit.](http://commits.kde.org/tokodon/198139f36c45bcf8cfff68aee5bc7845e0e8c3b5) 
+ Make new toot visibility icon match user choice. [Commit.](http://commits.kde.org/tokodon/cb57c7397991e05d49b7f63c817969d563f6ee6e) 
+ Add logo for gitlab. [Commit.](http://commits.kde.org/tokodon/b635bec093c2849086a075eb4f642bdfa1c98cd9) 
{{< /details >}}

{{< details title="vakzination" href="https://commits.kde.org/vakzination" >}}
+ Add ki18n_install. [Commit.](http://commits.kde.org/vakzination/4e281bdb0f8fb9a98754a7a9d850eff46bf2f824) 
+ Extract full page raster images in PDFs as well. [Commit.](http://commits.kde.org/vakzination/d52fdd6a7bd85bf334a9b2f21e2c5cfb8bae88b6) 
{{< /details >}}

