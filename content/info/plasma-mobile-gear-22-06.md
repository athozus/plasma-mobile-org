---
version: "22.06"
title: "Plasma Mobile Gear 22.06"
type: info
date: 2022-04-24
---

In this release following repositories are marked as unstable,

- kclock
- krecorder
- qmlkonsole
- tokodon
- plasmatube
- khealthcertificate
- vakzination

Some of this repositories are in process of kdereview, and will be marked stable in upcoming releases.

Following new repository is also added in this release,

- telly-skout

Following independantly released module is also included in this release,

- kweathercore

# Changelog

{{< details title="alligator" href="https://commits.kde.org/alligator" >}}
+ Add Qt6 CI. [Commit.](http://commits.kde.org/alligator/0212e125fa4a2929f4720b18aea8385803101ddb) 
+ Adapt build system to Qt6. [Commit.](http://commits.kde.org/alligator/a43274e3935a00e59c5cc86ac90fff15d2052f20) 
+ Link against sqlite on android. [Commit.](http://commits.kde.org/alligator/db0a2a2882a2110b05ac56dc072c0a5711e66663) 
+ Add missing license text. [Commit.](http://commits.kde.org/alligator/e39ea6d9a2d03669b34c2ea2146f12a2c41f1dc0) 
+ Add Android version handling. [Commit.](http://commits.kde.org/alligator/520f740cc719b8b4d3480234de66777e65188707) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/alligator/a4cb5604cddaaae5ac1d8866279835d432f14f57) 
{{< /details >}}

{{< details title="angelfish" href="https://commits.kde.org/angelfish" >}}
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/9de010458e554670f44347c57b37f0d8dfc023d6) 
+ Desktop: Decrease default window size. [Commit.](http://commits.kde.org/angelfish/c81997c4763bacb4779e583bfc4a95f56a7833c4) 
+ Fix raising window on Wayland. [Commit.](http://commits.kde.org/angelfish/5c2ebfb7eae23eb84f8191817413e3e99bc58764) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/159e850ca4aeff2b73586c6884994c4b2faf9eae) 
+ Update rust crates. [Commit.](http://commits.kde.org/angelfish/e5cd3171b4e9273685ad8507236a34922a7ac504) 
+ Allow to hide tab bar when there is a single tab. [Commit.](http://commits.kde.org/angelfish/ce7a5780352a32a71d5ddc561a4f1a408594896e) 
{{< /details >}}

{{< details title="audiotube" href="https://commits.kde.org/audiotube" >}}
+ PlaybackHistory: Load album covers asynchronously. [Commit.](http://commits.kde.org/audiotube/8860b62eec143eae111fcd163cd67b2f20e6044b) 
+ Fix crash on exit. [Commit.](http://commits.kde.org/audiotube/aadfc71200adf45fe03b01c5953c926673f50f29) 
+ Remove QT_QML_DEBUG. [Commit.](http://commits.kde.org/audiotube/16ce6562013df28d9c21808aaba3f8565a6050dd) 
+ Fix opening the 'Leoniden' artist page. [Commit.](http://commits.kde.org/audiotube/f7a4788e73e3d8a50d8eeb35c21f9af71d348ccb) 
+ Use std::invoke_result_t to simplify template code. [Commit.](http://commits.kde.org/audiotube/15425c7b019749746c23ced4cfef732d62110622) 
+ Fix up previous commit. [Commit.](http://commits.kde.org/audiotube/96c2a505a7118d4ab1e6f6f2ad52e037dbf6844a) 
+ Allow to show all favourites / history as a list. [Commit.](http://commits.kde.org/audiotube/b58e3fc2d43c35701b6bf66f87daef6825e2418f) 
+ Display AudioTube label just on mobile, and move the search bar back to the center. [Commit.](http://commits.kde.org/audiotube/4b1cbf11ad8abb2ab5a2ca19e0dcf3e5e9ab5701) 
+ App header: Move search bar to side, add app header, add search button for mobile. [Commit.](http://commits.kde.org/audiotube/1037ac8c025172fb45db0296da411bbb1860a091) 
+ Add loading indicator to minimized bar when loading track. [Commit.](http://commits.kde.org/audiotube/e48d6c8bb6aa9ab1d4addf90f7ae589ba6a1b914) 
+ Crop album art to square. [Commit.](http://commits.kde.org/audiotube/cb16ebb812e556df0c0dd95cdd4a76c7fa4af877) 
+ Asyncdatabase: Remove unused includes. [Commit.](http://commits.kde.org/audiotube/2c1df5e9e425679a02fa2cf53d57ac52cafacbfa) 
+ Asyncdatabase: Move parseRows into private API. [Commit.](http://commits.kde.org/audiotube/c3873d05ac1ca1c9ab376235a331973aedb186b0) 
+ Show mini progress bar when there is no space for the full one. [Commit.](http://commits.kde.org/audiotube/4fca0ca0c5c13fcf819228aebda67f0dc659c7d5) 
+ Asyncdatabase: Fix mistake in concept checks. [Commit.](http://commits.kde.org/audiotube/c02f369a0f751d90987771b7b1db1c266f97a9cb) 
+ Remove remove action from most viewed. [Commit.](http://commits.kde.org/audiotube/2e68ec0880e936bd4f558a9cf54c4e2018c25398) 
+ Asyncdatabase: Use concepts to define the interface of types that can be deserialized from SQL. [Commit.](http://commits.kde.org/audiotube/c7b22b348e3734b10bb2091ed8f169c540ab9257) 
+ Revamp media player UI. [Commit.](http://commits.kde.org/audiotube/f4c81722018d19d0b54eeb1a142e136cc0eab8d3) 
+ Make database query result deserialization more automatic. [Commit.](http://commits.kde.org/audiotube/9fa7757efd3422c20907652022fef9f61cc24635) 
+ Update ytmusicapi to 0.22.0. [Commit.](http://commits.kde.org/audiotube/f17987b3e786e85da5606e0bcfdd41faacf4c529) 
+ Library: Create database directory before opening database. [Commit.](http://commits.kde.org/audiotube/7c2bbf3a98ccb0e0356355c6dca38f25b7b70256) 
+ Asyncdatabase: Log error if database could not be opened. [Commit.](http://commits.kde.org/audiotube/4320921a9688f137e96090151bb26a23197a7b2d) 
+ Fix background color of the search popup. [Commit.](http://commits.kde.org/audiotube/306a4c866c2fecbafd26ffec502d60e482aadea9) 
+ Asyncdatabase: Move as much as possible out of the header. [Commit.](http://commits.kde.org/audiotube/2bfd1423007a2d0eb9b0ce799dc896fc76b52957) 
+ Run migrations in a transaction. [Commit.](http://commits.kde.org/audiotube/bec70c37db2c68999bb9d0249647c68bfbf9adca) 
+ Improve layout of the search popup. [Commit.](http://commits.kde.org/audiotube/a7e9588a90b2bf1ebe418abd6c83334f0cac216f) 
+ Add library feature. [Commit.](http://commits.kde.org/audiotube/463bdde0a054114dbe0f9e9b193b41dda3a5ee1b) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/audiotube/b7754e7d60e1e1b67e64125fffd30a237cc3feca) 
{{< /details >}}

{{< details title="calindori" href="https://commits.kde.org/calindori" >}}
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/calindori/bc346248a85f1aa9319df22f4816c68766b29793) 
{{< /details >}}

{{< details title="kalk" href="https://commits.kde.org/kalk" >}}
+ Show decimal separator according to locale. [Commit.](http://commits.kde.org/kalk/1efce29ac33f77b4c3fee5af97c26b9d6c8b33ce) 
+ Move config include to header. [Commit.](http://commits.kde.org/kalk/908ad5871af16ca87f4b57359edc2ef32f627ab5) 
+ Fix ',' input bug. [Commit.](http://commits.kde.org/kalk/7c7627cc35a989b853677de23347153b7bc6598c) 
{{< /details >}}

{{< details title="kasts" href="https://commits.kde.org/kasts" >}}
+ Fix padding and icon of sync password dialog. [Commit.](http://commits.kde.org/kasts/c5dab8131d522bc9619ed03864aa116db8441f82) 
+ Re-enable images in sync password dialog. [Commit.](http://commits.kde.org/kasts/a239bc922f0c2652a271a1d4b128d464a6eac36b) 
+ Make width of error list a bit wider to better see contents. [Commit.](http://commits.kde.org/kasts/5d8c81c6e18260ff0c84bd3f37dbbc0232edf979) 
+ Change default new podcast URL into placeholderText. [Commit.](http://commits.kde.org/kasts/e5cf95c8d7d902c7f52c77ce753e9e41c7bade5d) 
+ [Sync] Handle multiple entries having the same enclosure url. [Commit.](http://commits.kde.org/kasts/3383b6dfad76d00e415ff967256bee09c23c8828) 
+ Re-arrange mobile player and add extra row for toolbuttons. [Commit.](http://commits.kde.org/kasts/6930ff71d0db79a99ebd4adfdb79bfb70b82c324) 
+ Update appdata for upcoming release. [Commit.](http://commits.kde.org/kasts/10ced4cad9a8688ce98e1ba39fa268eaf1c02448) 
+ Add capability to define custom gpodder server. [Commit.](http://commits.kde.org/kasts/c4f12227a213c6ffd8b6ded0d5d208cf84a247c5) Implements feature [#454674](https://bugs.kde.org/454674)
+ Continue update of episodes even if gpodder server is unavailable. [Commit.](http://commits.kde.org/kasts/c44abc66ba5ee4a956a5aeae1d16223383e0dff2) 
+ Port away from deprecated mainItem on ScrollablePage. [Commit.](http://commits.kde.org/kasts/8ff1992d22154f49a9a5e343ff530898b03fbf6f) 
+ Improve gammarayability of EntriesModel & FeedsModel. [Commit.](http://commits.kde.org/kasts/3ae5306e56e17bb87e1fcdae5e7c366e6412acda) 
+ Add missing icons to src/CMakeLists.txt. [Commit.](http://commits.kde.org/kasts/27af0d907e23dc53c9bd905971cc1f3b6934b541) 
+ Properly apply version to AndroidManifest. [Commit.](http://commits.kde.org/kasts/6e89247c6eb3fa813b23c758699d47c7fe5086cb) 
+ Adjust time left on episodes according to current playback rate. [Commit.](http://commits.kde.org/kasts/e62d17f9b04034885a7fdf36ef604df1d15dc3db) Implements feature [#452135](https://bugs.kde.org/452135)
+ Update appdata for 22.06 release. [Commit.](http://commits.kde.org/kasts/f717473a8b5768dac2c0c8c1d2599be227a10f03) 
+ Refactor feed update routine to allow for entry, enclosure, authors and chapter updates. [Commit.](http://commits.kde.org/kasts/aac899a7f006678252a196471db475b81bd69afa) Fixes bug [#446158](https://bugs.kde.org/446158)
+ Add action to episode details page to go to the episode list for that subscription. [Commit.](http://commits.kde.org/kasts/6fb7118e34c5db021eb29e0a4d3e2add8914c52e) 
+ Use multithreading for feed updates (using ThreadWeaver). [Commit.](http://commits.kde.org/kasts/633f4fd0f0353103ad3635590354de7d95b59e72) Fixes bug [#452585](https://bugs.kde.org/452585)
+ Add new episodes to queue in ascending chronological order. [Commit.](http://commits.kde.org/kasts/2b363b6cec5b47be8b156ef19dcbcfc1dba378d4) 
+ Use embedded image in id3v2tag as fallback. [Commit.](http://commits.kde.org/kasts/b884ac69bf0720c918087a520f1573c37b1b90b0) 
+ Show refresh actions also on mobile. [Commit.](http://commits.kde.org/kasts/c42f0f3a00a96a64ad91a67119fea8e639a766b7) 
+ Decrease minimum window height. [Commit.](http://commits.kde.org/kasts/c5e286c17a84a09c0717cb81fee96c05af29f0b4) 
+ Update copyright date to 2022. [Commit.](http://commits.kde.org/kasts/2ceca4101b71f7509008ca83e35c8d93c112f455) 
+ Use include instead of forward decl for property types. [Commit.](http://commits.kde.org/kasts/6df9188cdc972996431d59482f73989f23856323) 
+ Fix KAboutData include. [Commit.](http://commits.kde.org/kasts/b6d5aa64701cc8adb93e2dac298934cde604845b) 
+ Adapt build system to Qt6. [Commit.](http://commits.kde.org/kasts/436912793427c4323abcd76195994ef7087dbc43) 
+ Do not process "other" fields before checking if entry already exists. [Commit.](http://commits.kde.org/kasts/cb9000faa5b1d83bd8f17f3a6de45639caaab632) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/kasts/5408e128fea7a611164c872144d57a6a99e765b6) 
{{< /details >}}

{{< details title="kclock" href="https://commits.kde.org/kclock" >}}
+ Fix page animation. [Commit.](http://commits.kde.org/kclock/e0a486b1c961485a7ee3bbb14239b6110f50fead) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/kclock/a91980fa9e0fcd6269c45e515c2264275125daea) 
{{< /details >}}

{{< details title="keysmith" href="https://commits.kde.org/keysmith" >}}
+ Port to standard Android version handling. [Commit.](http://commits.kde.org/keysmith/4157578daa937fd75b43ca63b598d8831e1747ee) 
+ Modernize window activation code. [Commit.](http://commits.kde.org/keysmith/dd1d7037dc0eefb277bc5167f381c1a04fae025c) 
+ Use proper gitlab CI jobs. [Commit.](http://commits.kde.org/keysmith/87cf7ea25cc283e85c4ecacf359e39f4877d429f) 
{{< /details >}}

{{< details title="khealthcertificate" href="https://commits.kde.org/khealthcertificate" >}}
+ It seems that version was not updated. [Commit.](http://commits.kde.org/khealthcertificate/d34cbfe7febee4ae9d58482777088f2089899b25) 
+ Fix find Qt<version>. [Commit.](http://commits.kde.org/khealthcertificate/830399613f7e45872e7e041920f3605188f98e9f) 
+ Add Qt6 and Qt6 Android CI. [Commit.](http://commits.kde.org/khealthcertificate/004d4d4e26c76f23c2725ca179b9aa1a5ac5ad4a) 
+ Build with LibreSSL as well. [Commit.](http://commits.kde.org/khealthcertificate/186bff6f6536ce8f8ca821d0bc55f9ce8df33483) 
+ Look for and use only the part of OpenSSL we actually need. [Commit.](http://commits.kde.org/khealthcertificate/17e3ca7e65dc72b3c11cc9ebafff4b9ecf7d1e6a) 
+ Allow to build against qt6. [Commit.](http://commits.kde.org/khealthcertificate/a1608c1c4e875f071aeefe0d6d2fb2bc0c3033d9) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/khealthcertificate/1398f18415528044a922314ba91ace2a843d52f0) 
{{< /details >}}

{{< details title="koko" href="https://commits.kde.org/koko" >}}
+ Simplify window activation code. [Commit.](http://commits.kde.org/koko/81fb9f73d36b9db3c85095e382e8032d368e905f) 
+ Always raise window on activation. [Commit.](http://commits.kde.org/koko/4af154959bf89bd9301944008bd8066c585b2a59) 
+ Fix parsing args when activating running instance. [Commit.](http://commits.kde.org/koko/ac38bf57995291d33157d8881a8593c0168de0b4) 
+ Use class name instead of instance for static method. [Commit.](http://commits.kde.org/koko/75707d5b789a58baba9642d195ee6253f0182a97) 
+ Run clang-format. [Commit.](http://commits.kde.org/koko/7c6cb7403f6334dd1df34288473d7c8ae16697ad) 
+ Properly enable QML debugging. [Commit.](http://commits.kde.org/koko/86b3148c4408f08e90c6e03d6611091d4462fa5c) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/koko/d3ff28711192eaa31e2d5689f5e48a4acf17aea5) 
{{< /details >}}

{{< details title="kongress" href="https://commits.kde.org/kongress" >}}
+ Fix reuse file info. [Commit.](http://commits.kde.org/kongress/249479afeac024621020d31b99ca568cce886e0c) 
+ Move all Android files to android/. [Commit.](http://commits.kde.org/kongress/2d6a7349f2e04cfdc16c6110ff8a76d9ac08aa43) 
+ Add Android version handling. [Commit.](http://commits.kde.org/kongress/aba5445def47295983e1f6caf9356e343afb4a21) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/kongress/92a20641e2a0c2934627551b421a5108550cfc6c) 
{{< /details >}}

{{< details title="krecorder" href="https://commits.kde.org/krecorder" >}}
+ Add Android version handling. [Commit.](http://commits.kde.org/krecorder/06cce4fafd3dcaa57829be5260749ba12e4a37c6) 
+ Fix incorrect application icon being used. [Commit.](http://commits.kde.org/krecorder/1f3d4068a14ef4bf595c3dae0b36a7c94bb7836b) 
+ Fix settings not resizing properly to contents. [Commit.](http://commits.kde.org/krecorder/35c1f97d66d8773b79f62fb17e622ff028cb78e0) 
+ Fix missing icon in about page and inconsistent app metadata #19. [Commit.](http://commits.kde.org/krecorder/82df4b3165860cfd9e60c9af68e789c2db8fbae5) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/krecorder/9d738e4b913ba92b21cf388a879bb80ff8672c4f) 
{{< /details >}}

{{< details title="ktrip" href="https://commits.kde.org/ktrip" >}}
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/ktrip/d76cee6cb27174cd1c5fccce4622c8dfba6bf7d1) 
{{< /details >}}

{{< details title="kweather" href="https://commits.kde.org/kweather" >}}
+ Add missing license text. [Commit.](http://commits.kde.org/kweather/aa8962ad9722fc7517b47bcd91abfce4350cb662) 
+ Add Android version handling. [Commit.](http://commits.kde.org/kweather/da7833b36649d9d22bb69888f1325ae1efba4abe) 
+ Use org.kde.breeze style on Android. [Commit.](http://commits.kde.org/kweather/2ead5c4821d82ade1d536c503fe4aa59ff3b7472) 
+ Use locally computed sunrise and lunar phase data. [Commit.](http://commits.kde.org/kweather/98a2526151e5fd8c259557a07a6f29fd701da6b2) 
+ Fix bug address. [Commit.](http://commits.kde.org/kweather/6017d9fcaa6e0082da3f1efa153df9a18661b904) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/kweather/0bffcccf1e28f3d3e86cdf7fbbd43fc586728270) 
{{< /details >}}

{{< details title="neochat" href="https://commits.kde.org/neochat" >}}
+ Don't use PublicRoomsChunk::aliases. [Commit.](http://commits.kde.org/neochat/efae510fdaff7354f18be0760bc5a18151a291e9) 
+ Fix the switch room direction. [Commit.](http://commits.kde.org/neochat/49c9c63bf5b9f1f05af4c9c2c1c0b8e0d40df9be) 
+ Clear the text from the user list filter in the room drawer when the room is changed. [Commit.](http://commits.kde.org/neochat/90cee0f4377ae7cff23344151c8397b7154f2191) 
+ It's 2022. [Commit.](http://commits.kde.org/neochat/f9a96ccdabb9129360f17c0f3404c6d2b509b920) 
+ Fixe Reply or Edit from Chatbar. [Commit.](http://commits.kde.org/neochat/9c2e0669f6ac1783f289b7148fabe232dd10ea59) Fixes bug [#455016](https://bugs.kde.org/455016)
+ Compact Mode improvements. [Commit.](http://commits.kde.org/neochat/083a2f9772b87179c30a53a2929bc3115aaa8e33) Fixes bug [#454897](https://bugs.kde.org/454897)
+ Make Right Click on Room bring up Context Menu. [Commit.](http://commits.kde.org/neochat/b44e81c849ad9593ca94e4d57007ae0fd3de59ee) Fixes bug [#454892](https://bugs.kde.org/454892)
+ For all html messages \n needs to be replaces with <br> or the linebreaks are lost. [Commit.](http://commits.kde.org/neochat/ede860c99f0afa1b9de179087abc45190a6b8861) 
+ Fix fix. [Commit.](http://commits.kde.org/neochat/525015fe7895e1390493613186efefd65832d791) 
+ Don't show notifications if application is active and same room is active. [Commit.](http://commits.kde.org/neochat/b834510be0995b364aab08ba2331a4810b0e73a5) 
+ Fix hoverActions. [Commit.](http://commits.kde.org/neochat/870061123549c40edf6f6c1501d3e17143fe052f) 
+ Fix custom emoji creation. [Commit.](http://commits.kde.org/neochat/7bd4aac692cadd1a1ee35b3d4989844473c81334) 
+ Add automatic room sidebar hiding option. [Commit.](http://commits.kde.org/neochat/db5e3288690631a648fe9c5888cca17b0305c3a7) 
+ Add space after an autocomplete. [Commit.](http://commits.kde.org/neochat/29816730e432da02a4773a29bcc17c4c92920f20) 
+ Fix compilation against libQuotient 0.6. [Commit.](http://commits.kde.org/neochat/7214936eaa1730a7cc733f41f4d31e08fe38b8ea) 
+ Revert "Linkify urls". [Commit.](http://commits.kde.org/neochat/5a7c3295dc3d81b11da80c50b9fc7658e47262d0) 
+ Adapt to libQuotient API changes. [Commit.](http://commits.kde.org/neochat/dce4a409c7aa707a305db1e2a1c584cb269d3f97) 
+ Fix XML. [Commit.](http://commits.kde.org/neochat/070fe45a2dc9a543a69431a3a979ce3d7b142f0a) 
+ Update bug reporting urls. [Commit.](http://commits.kde.org/neochat/fb9183e5c323a2401712824183aab3ddb2ac94e9) 
+ Adds some basic mouse contorls to the quickswitcher. The icons can now be clicked to select the room and the highlight is moved to the current hovered room. [Commit.](http://commits.kde.org/neochat/e62288e6f17466cd7c3dda77f2213d91c25fe33e) 
+ Appstream: define launchable. [Commit.](http://commits.kde.org/neochat/4f978a950baa5c41cf12df1a6feb449fa93c5ec0) 
+ Linkify urls. [Commit.](http://commits.kde.org/neochat/1763dc13c5c827c55e707ed2b0f965ff43d11386) 
+ Don't crash while trying to load last message by own user. [Commit.](http://commits.kde.org/neochat/0d00d4200c7b5f521b6932bb2630abb607cbeffb) 
+ Don't escape html while posting messages. [Commit.](http://commits.kde.org/neochat/77e20ec4464601940610248e8eff29e52f80fe82) 
+ Revert "Fix double quoting and missing new lines in message sent". [Commit.](http://commits.kde.org/neochat/101b57c581d4981e08a960b9ee10abe6e619c8a1) 
+ Fix rasing window when activating notifications. [Commit.](http://commits.kde.org/neochat/4e61c5e53c5cc80581641d6092351e2d8fa90d31) 
+ Always send messages as HTML. [Commit.](http://commits.kde.org/neochat/6871ed051c1f30c79e838bbd25b7110dc4d1462d) 
+ Fix search item being behind the roomlist in collapsed mode by moving code into ListView. Now the search item is always at the top of the list. [Commit.](http://commits.kde.org/neochat/10da870ab3e50e65d70e3d4d0ff1faf4708568f1) 
+ Fix scrollbar behaviour in Room List. [Commit.](http://commits.kde.org/neochat/cef5d11130e73e4585c8339252df53a51d186ab0) 
+ Allow disabling sending of typing notifications. [Commit.](http://commits.kde.org/neochat/5595d8f896fc5d5236d8033e8923dbd06de65d92) 
+ Fix typo. [Commit.](http://commits.kde.org/neochat/1bcff6503f2c3c668212c050c87366e99e8d2111) 
+ Remove leftover spellchecking files. [Commit.](http://commits.kde.org/neochat/98571cb37d2bf3aaf8c856e7bc4e28990dda524d) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/neochat/1f551b5f593cce891256055434a9b882461e8c9e) 
{{< /details >}}

{{< details title="plasma-dialer" href="https://commits.kde.org/plasma-dialer" >}}
+ Kde-telephony-daemon: cleanup CMakeLists.txt. [Commit.](http://commits.kde.org/plasma-dialer/7ae77ca6b267124178c4ce4af49fbcb70f17d057) 
+ Lower KDE_COMPILERSETTINGS_LEVEL. [Commit.](http://commits.kde.org/plasma-dialer/4c217e6b03de2d80cd5e48b061f35190004c9bc3) 
+ Use imported target for mprisqt. [Commit.](http://commits.kde.org/plasma-dialer/df71d2c731e551b98792e70cd266c77030fc81c3) 
{{< /details >}}

{{< details title="plasma-phonebook" href="https://commits.kde.org/plasma-phonebook" >}}
+ Clean up and fix KAboutData. [Commit.](http://commits.kde.org/plasma-phonebook/0d0d23867c304fb8e3c58828320130ddc5eede1b) 
+ Port to Kirigami.Avatar. [Commit.](http://commits.kde.org/plasma-phonebook/89d3c101d98ca7b92f111486bca19726bfe37841) 
{{< /details >}}

{{< details title="plasma-settings" href="https://commits.kde.org/plasma-settings" >}}
+ [cellularnetwork] Use same method of toggling mobile data as shell. [Commit.](http://commits.kde.org/plasma-settings/bf57e2933ebde8187edda480c1220975cbf533d7) 
+ [cellularnetwork] Fix sim lock screen card positioning. [Commit.](http://commits.kde.org/plasma-settings/61d77ad600ca30767b9b5a912a81598ad4412a80) 
+ Use keywords when searching. [Commit.](http://commits.kde.org/plasma-settings/e4fabe55ea9f0be6d8289ea51650c6f0e347a187) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/plasma-settings/70cdf6ffaa07d86134c521185a845b8c08e55ba0) 
{{< /details >}}

{{< details title="qmlkonsole" href="https://commits.kde.org/qmlkonsole" >}}
+ Formatting. [Commit.](http://commits.kde.org/qmlkonsole/2fd4b4e3439b95b8fd47902fcb9f185e4c3beffd) 
+ Ensure arrow keys won't pass the string component. [Commit.](http://commits.kde.org/qmlkonsole/f56112bbf227977cc556088625ef07d5f6cc32c4) 
+ Fix bar and tilde toolbar buttons not working. [Commit.](http://commits.kde.org/qmlkonsole/0d90427785f80aacad1b28f3e8b9226f1f46b15f) 
+ Fix button colour contrast. [Commit.](http://commits.kde.org/qmlkonsole/3bb424fed105d7967b80f0d7e9f0a5910aa10e4d) 
+ Update README. [Commit.](http://commits.kde.org/qmlkonsole/e73f26dfd4921a073cd0d557d3dbcc401c6f0557) 
+ Fix terminal button text being elided. [Commit.](http://commits.kde.org/qmlkonsole/46b4af48b58f29435b92d7505e9bca69f841f23c) 
+ Use swipe gesture for page navigation. [Commit.](http://commits.kde.org/qmlkonsole/2548bd92467d0e110a65ad212f4533f2f9c9f251) 
+ Improve saved commands UX. [Commit.](http://commits.kde.org/qmlkonsole/8fa8977a13d03df6446b8efe5b7d56fbb325021b) 
+ Update .kde-ci.yml. [Commit.](http://commits.kde.org/qmlkonsole/568d7a6450345b7b8943e09fd23554a191478095) 
+ Add about page #12. [Commit.](http://commits.kde.org/qmlkonsole/0a8ac8a40e27193649dad397b42d8f9a03d4b2a3) 
+ Move QuickActionSettings.qml to correct folder. [Commit.](http://commits.kde.org/qmlkonsole/d23e22097a46029c479bde2c98f2e7864dbca11c) 
+ Port to Kirigami.Dialog. [Commit.](http://commits.kde.org/qmlkonsole/dd147f09c9bf17395c8acf5685bda49a80e00b58) 
+ Do not ignore po/ files. [Commit.](http://commits.kde.org/qmlkonsole/c965666e354a5f1a6c0a2e0529111a220d8d621a) 
{{< /details >}}

{{< details title="spacebar" href="https://commits.kde.org/spacebar" >}}
+ Alternatives for unsupported WSP content types. [Commit.](http://commits.kde.org/spacebar/8d98c36a26d845897a4cb31612399e4ad16215c6) 
+ Fix typo in method name. [Commit.](http://commits.kde.org/spacebar/90d63ab550c0d7d804f7ad099c5c60b7f7ff3892) 
+ Messagemodel: Use QUrl for url manipulation. [Commit.](http://commits.kde.org/spacebar/b58828ec30cd02dfc930d0429438351e91663202) 
+ Fix param field usage from commit 265610e7. [Commit.](http://commits.kde.org/spacebar/6dc7e03b862f03c3b89987372ca52615c4f47d24) 
+ Use ApplicationLauncherJob to open plasma-phonebook. [Commit.](http://commits.kde.org/spacebar/a63410cace2c4826e0b9005504b225d5b3a04fc7) 
+ Make VCardConverter non-static. [Commit.](http://commits.kde.org/spacebar/d30d8e8ee30ac7f892f5aefa0514a3dca3336900) 
+ Use destructuring syntax for tuples. [Commit.](http://commits.kde.org/spacebar/485a34e042d121b39d6b5d8f16960515d47f0d70) 
+ Add missing connectFuture call. [Commit.](http://commits.kde.org/spacebar/67f66238fac5855d3169ace8d1670c2de06edaee) 
+ Use std::array to store arrays of constants. [Commit.](http://commits.kde.org/spacebar/265610e7d6789f17c3d9e39400d36ffb98ef6b9c) 
+ Refactor AsyncDatabase to use QFuture return types. [Commit.](http://commits.kde.org/spacebar/96a8b383c6265d191469610d75392988484204c0) 
{{< /details >}}

### [telly-skout](https://commits.kde.org/telly-skout)

+ New in this release

{{< details title="tokodon" href="https://commits.kde.org/tokodon" >}}
+ Add Android version handling. [Commit.](http://commits.kde.org/tokodon/65844307d3d9bd20a53ff1ebc5b8946f5842c858) 
+ Add Android app icon. [Commit.](http://commits.kde.org/tokodon/c80bd74887287df5c46440bd114ef9d6c05a2613) 
+ Create debug categories. [Commit.](http://commits.kde.org/tokodon/2358332760488d3cf0f3da8796a9dc51c6741c1e) 
+ Support nextcloud social. [Commit.](http://commits.kde.org/tokodon/3f65653bf9e6f115fa9ed61fdf2ae806289f21bf) 
+ Fix activating running instance on Wayland. [Commit.](http://commits.kde.org/tokodon/2f3ed5c2259ffd37768b0213f439c5ab2fba3fbc) 
+ Add config for showing link preview. [Commit.](http://commits.kde.org/tokodon/61bfb41171f66141669238942ede70985c1808c1) 
+ Don't show attachement and card directly when the content is not visible. [Commit.](http://commits.kde.org/tokodon/58286a5da4a2ad37031afdd1823c4e4a156d578e) 
+ Add link hovering. [Commit.](http://commits.kde.org/tokodon/a1152c221ab853af0828760a0da8d01ff271716a) 
+ Allow filtering notifications. [Commit.](http://commits.kde.org/tokodon/96968f5c6337e44c7b9f959beb99a4054d5e32a4) 
+ Start implementing notification view. [Commit.](http://commits.kde.org/tokodon/c0a38aa2d3a3676e1cf1e41524b50f1e667d0857) 
+ Fix minor typo. [Commit.](http://commits.kde.org/tokodon/7095fb917233cfd31f7144c18f45d2399f3b32b9) 
+ Support desktop notification. [Commit.](http://commits.kde.org/tokodon/f23b509cd63b880c3c279323f9d8c02594db85d6) 
+ Add support for cards. [Commit.](http://commits.kde.org/tokodon/745ec0d326a19e0f433de2e4c96045c52247a617) 
+ Fix rendering of reblog. [Commit.](http://commits.kde.org/tokodon/aa6aa1230247f18145d49fbccf2d2253414d64fd) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/tokodon/f5596a3f63a56e089e6bb7010fa809b40e96a3e4) 
{{< /details >}}

{{< details title="vakzination" href="https://commits.kde.org/vakzination" >}}
+ Add windows CI. [Commit.](http://commits.kde.org/vakzination/74a99e5c790c91c1b0528eefc55d80183716f68a) 
+ Use proper reuse CI job. [Commit.](http://commits.kde.org/vakzination/04575458dbbc30ac9fa5fe2ea7638de7a74769ac) 
{{< /details >}}

