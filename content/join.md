---
title: Community
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    weight: 5
sassFiles:
  - scss/join.scss
header_image_link: /img/konqi/konqi-contribute.png
---

If you would like to help contribute, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

See more information on the project's [source code here](/get-source).

**Community channels:**

* [![](/img/matrix.svg) Matrix (most active with developers)](https://matrix.to/#/#plasmamobile:matrix.org) 

* [![](/img/kde.png) KDE Discuss (Forum)](https://discuss.kde.org)

* [![](/img/telegram.svg) Telegram](https://t.me/plasmamobile)

* [![](/img/mail.svg) Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

**Related project channels:**

* [![](/img/mail.svg) Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
